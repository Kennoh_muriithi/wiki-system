from django.conf.urls import re_path,include
from . import views

urlpatterns = [
    re_path(r'^sign-in/', views.sign_in, name='sign_in'), 
    re_path(r'^sign-out/', views.sign_out, name='sign_out'), 
]