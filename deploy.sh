echo "Deploy script started"
cd /home/project/wiki-system
git pull origin master
pip install -r requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
supervisorctl restart all
echo "Deploy script finished execution"