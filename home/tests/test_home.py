from django.test import Client
from django.urls import reverse
from django.contrib.auth.models import Group, User
import pytest
from django.urls import reverse
from django.test import TestCase, Client
from home.models import WikiSearch

client = Client()


@pytest.mark.django_db
def test_home_urls():
    response = client.get(reverse('home'))
    assert(response.status_code, 200)
    # response = client.get(reverse('new_page'),args=[client.unique_id])
    # assert(response.status_code, 200)
    # response = client.get(reverse('edit_wiki_object',args=['unique_id']))
    # assert(response.status_code, 200)
    # response = client.get(reverse('fetch_description',args=['unique_id']))
    # assert(response.status_code, 200)


@pytest.mark.django_db
def test_search_urls(request):
    search  = WikiSearch.objects.create(title = 'Test Title',external_link='https://www.tests.com',description='A sample description')
    response = client.get(reverse('search'))
    assert search.count() == 1
    # assert(response.status_code, 200)
