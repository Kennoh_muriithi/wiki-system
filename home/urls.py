from django.conf.urls import include, re_path
from . import views

urlpatterns = [
    re_path(r'^view-details/(?P<unique_id>[\w-]+)/$', views.new_page, name='new_page'),
    re_path(r'^edit-wiki-object/(?P<unique_id>[\w-]+)/$', views.edit_wiki_object, name='edit_wiki_object'),
    re_path(r'^fetch-description/(?P<unique_id>[\w-]+)/$', views.fetch_description, name='fetch_description'),
    re_path(r'^search', views.search, name='search'), 
    re_path(r'^$', views.home, name='home'), 
]



